package net.larboard.lib.error.exception.impl;

import lombok.Getter;
import net.larboard.lib.error.exception.CoreRuntimeException;
import org.springframework.http.HttpStatus;

public class ConflictException extends CoreRuntimeException {
    @Getter
    private final HttpStatus httpStatus = HttpStatus.CONFLICT; // don't make static: breaks inherited abstract getter

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflictException(Throwable cause) {
        super(cause);
    }
}
