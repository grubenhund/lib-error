package net.larboard.lib.error.exception.impl;

import lombok.Getter;
import net.larboard.lib.error.exception.CoreRuntimeException;
import org.springframework.http.HttpStatus;

public class UploadInvalidException extends CoreRuntimeException {
    @Getter
    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST; // don't make static: breaks inherited abstract getter

    public UploadInvalidException(String message) {
        super(message);
    }

    public UploadInvalidException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadInvalidException(Throwable cause) {
        super(cause);
    }
}
